package Main.model;

import javax.persistence.*;

@Entity
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "person_document_type_id")
    private PersonDocumentType personDocumentType;

    private String name;

    @Column(name = "document_format")
    private String documentFormat;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormat() {
        return documentFormat;
    }

    public void setFormat(String documentFormat) {
        this.documentFormat = documentFormat;
    }

    public PersonDocumentType getPersonDocumentType() {
        return personDocumentType;
    }
}

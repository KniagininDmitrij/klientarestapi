package Main.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonDocumentTypeRepository extends CrudRepository<PersonDocumentType, Integer> {
}

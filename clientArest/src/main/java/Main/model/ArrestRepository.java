package Main.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArrestRepository extends CrudRepository<ArrestDB, Integer> {
}

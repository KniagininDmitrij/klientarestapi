package Main.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ClientKey implements Serializable {
    @Column(name = "last_name", length = 100)
    private String lastName;

    @Column(name = "first_name", length = 100)
    private String firstName;

    @Column(name="doc_type_id")
    private int documentTypeId;

    @Column(name = "document_number_and_series")
    private String numberAndSeries;

    public ClientKey(){

    }
    public ClientKey(String lastName, String firstName, int documentTypeId, String numberAndSeries) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.documentTypeId = documentTypeId;
        this.numberAndSeries = numberAndSeries;
    }

    public ClientKey(Client client){
        this(client.getLastName(), client.getFirstName(), client.getDocumentTypeId(), client.getNumberAndSeries());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientKey clientKey = (ClientKey) o;
        return Objects.equals(lastName, clientKey.lastName) && Objects.equals(firstName, clientKey.firstName) && Objects.equals(documentTypeId, clientKey.documentTypeId) && Objects.equals(numberAndSeries, clientKey.numberAndSeries);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName, firstName, documentTypeId, numberAndSeries);
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public int getDocumentTypeId() {
        return documentTypeId;
    }

    public String getNumberAndSeries() {
        return numberAndSeries;
    }
}

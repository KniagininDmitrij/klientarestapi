package Main.model;

public enum ArrestState {
    ACTIVE, FULFILLED, CANCELED
}

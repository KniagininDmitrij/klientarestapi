package Main.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "clients")
public class Client {

    @EmbeddedId
    private ClientKey key;

    @Column(name = "last_name", insertable = false, updatable = false, length = 100)
    private String lastName;

    @Column(name = "first_name", insertable = false, updatable = false, length = 100)
    private String firstName;

    @Column(name="doc_type_id", insertable = false, updatable = false)
    private int documentTypeId;

    @Column(name = "document_number_and_series", insertable = false, updatable = false)
    private String numberAndSeries;

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date birthday;

    @Column(name = "birth_place")
    private String birthPlace;

    @OneToMany(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumns({
            @PrimaryKeyJoinColumn(name = "last_name", referencedColumnName = "client_last_name"),
            @PrimaryKeyJoinColumn(name = "first_name", referencedColumnName = "client_first_name"),
            @PrimaryKeyJoinColumn(name = "doc_type_id", referencedColumnName = "client_doc_type_id"),
            @PrimaryKeyJoinColumn(name = "document_number_and_series", referencedColumnName = "client_number_and_series"),
    })
    private List<ArrestDB> arrests;

    public List<ArrestDB> getArrests() {
        return arrests;
    }

    public void setArrests(List<ArrestDB> arrests) {
        this.arrests = arrests;
    }

    public ClientKey getKey() {
        return key;
    }

    public void setKey(ClientKey key) {
        this.key = key;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(int documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    public String getNumberAndSeries() {
        return numberAndSeries;
    }

    public void setNumberAndSeries(String numberAndSeries) {
        this.numberAndSeries = numberAndSeries;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

}

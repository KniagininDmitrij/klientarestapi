package Main.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "arrests")
public class ArrestDB {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int departmentId;

    @Column(name = "document_date")
    private Date documentDate;

    @Column(name = "primary_document_number")
    private String primaryDocumentNumber;

    @Column(name = "arrest_base_def", length = 1000)
    private String arrestBaseDefinition;

    private long amount;

    @Enumerated(EnumType.STRING)
    private ArrestState arrestState;

    @Column(length = 100)
    private String clientLastName;
    @Column(length = 100)
    private String clientFirstName;
    private int clientDocumentTypeId;
    private String clientNumberAndSeries;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public Date getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(Date documentDate) {
        this.documentDate = documentDate;
    }

    public String getPrimaryDocumentNumber() {
        return primaryDocumentNumber;
    }

    public void setPrimaryDocumentNumber(String primaryDocumentNumber) {
        this.primaryDocumentNumber = primaryDocumentNumber;
    }

    public String getArrestBaseDefinition() {
        return arrestBaseDefinition;
    }

    public void setArrestBaseDefinition(String arrestBaseDefinition) {
        this.arrestBaseDefinition = arrestBaseDefinition;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public ArrestState getArrestState() {
        return arrestState;
    }

    public void setArrestState(ArrestState arrestState) {
        this.arrestState = arrestState;
    }

    public String getClientLastName() {
        return clientLastName;
    }

    public void setClientLastName(String clientLastName) {
        this.clientLastName = clientLastName;
    }

    public String getClientFirstName() {
        return clientFirstName;
    }

    public void setClientFirstName(String clientFirstName) {
        this.clientFirstName = clientFirstName;
    }

    public int getClientDocumentTypeId() {
        return clientDocumentTypeId;
    }

    public void setClientDocumentTypeId(int clientDocumentTypeId) {
        this.clientDocumentTypeId = clientDocumentTypeId;
    }

    public String getClientNumberAndSeries() {
        return clientNumberAndSeries;
    }

    public void setClientNumberAndSeries(String clientNumberAndSeries) {
        this.clientNumberAndSeries = clientNumberAndSeries;
    }
}

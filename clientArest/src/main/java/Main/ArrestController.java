package Main;

import Main.model.ArrestDB;
import Main.request.InputArrestRequest;
import Main.response.Result;
import Main.service.ArrestManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArrestController {
    public static final String ROOT_URL = "/arrests/";
    public static final int SUCCESS_CODE = 0;
    public static final int FAILED_CODE = 5;
    public static final int BAD_DATA_CODE = 3;
    public static final int DEFAULT_RESULT_ARREST_ID = 0;

    @Autowired
    ArrestManager arrestManager;
    @PostMapping(ROOT_URL)
    public Result add(@RequestBody InputArrestRequest inputArrestRequest){
        try {
            ArrestDB arrestDB = arrestManager.getArrestObjectFromRequest(inputArrestRequest);
            return new Result(arrestDB.getId(), SUCCESS_CODE, "");
        }catch (IllegalArgumentException illegalArgumentException){
            return new Result(DEFAULT_RESULT_ARREST_ID, BAD_DATA_CODE, illegalArgumentException.getMessage());
        }
        catch (Exception exception){
            return new Result(DEFAULT_RESULT_ARREST_ID, FAILED_CODE, exception.getMessage());
        }

    }
}

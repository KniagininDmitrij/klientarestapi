package Main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Map;

@SpringBootApplication
public class Main {
    public static final String CONFIG_PATH = "application.yml";

    public static void main(String[] args) {
        Yaml yaml = new Yaml();
        SpringApplication app = new SpringApplication(Main.class);

        try(InputStream configFileInputStream = new FileInputStream(CONFIG_PATH)){
            Map<String, Object> configMap = yaml.load(configFileInputStream);
            app.setDefaultProperties(Collections
                    .singletonMap("server.port",
                            ((Map<String, String>)(configMap.get("server"))).get("port")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        app.run(args);
    }
}

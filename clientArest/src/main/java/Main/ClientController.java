package Main;

import Main.model.Client;
import Main.model.ClientKey;
import Main.model.ClientRepository;
import Main.service.DBManager;
import Main.service.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientController {
    private static final String ROOT_URL = "/clients/";

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    DBManager dbManager;

    @PostMapping(ROOT_URL)
    public int add(Client client){
        String docFormat = dbManager.getIdenDocFormatByPersonDocumentTypeID(client.getDocumentTypeId())
                .getFormat();
        if (Validator.identDocFormatValidate(client.getNumberAndSeries(), docFormat)){
            client.setKey(new ClientKey(client));
            return clientRepository.save(client).getKey().hashCode();
        }
        return ResponseEntity.status(403).body(null).getStatusCodeValue();
    }
}

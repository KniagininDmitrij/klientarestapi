package Main.response;

public class Result {
    private int arrestID;
    private int resultCode;
    private String resultText;

    public Result(int arrestID, int resultCode, String resultText) {
        this.arrestID = arrestID;
        this.resultCode = resultCode;
        this.resultText = resultText;
    }

    public Result() {
    }

    public int getArrestID() {
        return arrestID;
    }

    public void setArrestID(int arrestID) {
        this.arrestID = arrestID;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultText() {
        return resultText;
    }

    public void setResultText(String resultText) {
        this.resultText = resultText;
    }
}

package Main.service;

import Main.model.*;
import Main.request.InputArrestRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class DBManager {
    @Autowired
    PersonDocumentTypeRepository personDocumentTypeRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    ArrestRepository arrestRepository;
    @Autowired
    DepartmentRepository departmentRepository;

    private static Map<Integer, Department> IdenDocFormatCache = new HashMap<>();
    private static Map<Integer, PersonDocumentType> IdenPersonDocFormatCache = new HashMap<>();

    public Department getIdenDocFormatByDepartmentTypeID(int type) {
        return IdenDocFormatCache.computeIfAbsent(type, t -> {
            Optional<Department> optionalDepartment = departmentRepository.findById(type);
            if (optionalDepartment.isPresent()){
                return optionalDepartment.get();
            }else{
                throw new IllegalArgumentException("Unknown identification document type, check input data!!!");
            }
        });
    }

    public PersonDocumentType getIdenDocFormatByPersonDocumentTypeID(int type) {
        return IdenPersonDocFormatCache.computeIfAbsent(type, t -> {
            Optional<PersonDocumentType> optionalPersonDocumentType = personDocumentTypeRepository.findById(type);
            if (optionalPersonDocumentType.isPresent()){
                return optionalPersonDocumentType.get();
            }else{
                throw new IllegalArgumentException("Unknown identification document type, check input data!!!");
            }
        });
    }

    public ClientKey getClientOrCreateAndGet(InputArrestRequest inputArrestRequest, String innerFormattedNS) {
        ClientKey clientKey = new ClientKey(inputArrestRequest.getLastName(), inputArrestRequest.getFirstName(),
                inputArrestRequest.getIdentDoc().getType(), innerFormattedNS);
        Optional<Client> optionalClient = clientRepository.findById(clientKey);
        if (optionalClient.isPresent()){
            return clientKey;
        }else{
            Client client = getNewClientFromRequest(inputArrestRequest);
            client.setKey(clientKey);
            return clientRepository.save(client).getKey();
        }
    }

    private Client getNewClientFromRequest(InputArrestRequest input) {
        Client client = new Client();
        client.setFirstName(input.getFirstName());
        client.setLastName(input.getLastName());
        client.setDocumentTypeId(input.getIdentDoc().getType());
        client.setNumberAndSeries(input.getIdentDoc().getNumberSeries());
        return client;
    }

    public ArrestDB saveNewArrest(ArrestDB newArrestDB) {
        return arrestRepository.save(newArrestDB);
    }

    public ArrestDB getSameArrestByClientKeyAndCurrentArrest(ClientKey clientKey, ArrestDB newArrest, String refDocNum) {
        Optional<Client> optionalClient = clientRepository.findById(clientKey);
        if (optionalClient.isPresent()){
            Client client = optionalClient.get();
            List<ArrestDB> arrestDBList = client.getArrests();
            ArrestDB sameArrestDB = null;
            for (ArrestDB clientArrest : arrestDBList){
                if (isSameArrests(newArrest, clientArrest, refDocNum)){
                    sameArrestDB = clientArrest;
                    break;
                }
            }
            if (sameArrestDB == null){
                throw new IllegalArgumentException("Same Arrest for edit or cancel is not find, try to change operation to Initial");
            }
            return sameArrestDB;

        }else throw new IllegalArgumentException("Client with same Arrest not find!!!");
    }

    private boolean isSameArrests(ArrestDB newArrest, ArrestDB clientArrest, String refDocNum) {
        boolean isSame = clientArrest.getPrimaryDocumentNumber().equals(refDocNum);
        return isSame;
    }

    public ArrestDB updateArrest(ArrestDB sameArrest) {
        return arrestRepository.save(sameArrest);
    }

    public Client getClient(ClientKey clientKey) {
        return clientRepository.findById(clientKey).get();
    }

    public void addArrest2Client(ClientKey clientKey, ArrestDB newArrest) {
        Client client = getClient(clientKey);
        List<ArrestDB> clientArrests = client.getArrests();
        if (clientArrests == null){
            clientArrests = new ArrayList<>();
        }
        clientArrests.add(newArrest);
        client.setArrests(clientArrests);
        clientRepository.save(client);
    }
}

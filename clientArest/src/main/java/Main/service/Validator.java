package Main.service;

import Main.model.Department;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    public static final String FIELD_REGEX_TEMPLATE = "[0-9]{%d}";
    public static final String FIELD_FORMAT_REGEX = "[NS]+";
    private static final Pattern FIELD_FORMAT_PATTERN = Pattern.compile(FIELD_FORMAT_REGEX);

    private static Map<String, String> regexCache = new HashMap<>();

    public static boolean identDocFormatValidate(String numSeries, String format){
        String formatRegex = getRegexForFormatString(format);
        return numSeries.matches(formatRegex);
    }

    private static String getRegexForFormatString(String format) {
        return regexCache.computeIfAbsent(format, f ->{
            Matcher matcher = FIELD_FORMAT_PATTERN.matcher(f);
            String resultRegex = f;
            while (matcher.find()) {
                int start = matcher.start();
                int end = matcher.end();
                int fieldSize = end-start;
                resultRegex = resultRegex.replace(f.substring(start, end), String.format(FIELD_REGEX_TEMPLATE, fieldSize));
            }
            return resultRegex;
        });
    }

    public static String reformat(String numberSeries, Department department) {
        String fromFormat = department.getFormat();
        String formatted = department.getPersonDocumentType().getFormat();

        for (int i = 0; i < fromFormat.length(); i++){
            char item = fromFormat.charAt(i);
            if (item == 'N' || item == 'S'){
                formatted = formatted.replaceFirst(item+"", numberSeries.charAt(i)+"");
            }
        }
        return formatted;
    }
}

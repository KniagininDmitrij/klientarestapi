package Main.service;

import Main.model.ArrestDB;
import Main.model.ArrestState;
import Main.model.ClientKey;
import Main.model.Department;
import Main.request.Arrest;
import Main.request.IdentDoc;
import Main.request.InputArrestRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ArrestManager {
    public static final int INITIAL = 1;
    public static final int EDIT = 2;
    public static final int CANCEL = 3;

    @Autowired
    DBManager dbManager;

    public ArrestDB getArrestObjectFromRequest(InputArrestRequest inputArrestRequest){
        IdentDoc identDoc = inputArrestRequest.getIdentDoc();
        Arrest arrest = inputArrestRequest.getArrest();
        Department department = dbManager.getIdenDocFormatByDepartmentTypeID(inputArrestRequest.getOrganCode());

        String format = department.getFormat();
        String numberSeries = identDoc.getNumberSeries();

        ArrestDB newArrest;
        if (Validator.identDocFormatValidate(numberSeries, format)){
            String innerFormattedNumberAndSeries = Validator.reformat(numberSeries, department);
            ClientKey clientKey = dbManager.getClientOrCreateAndGet(inputArrestRequest, innerFormattedNumberAndSeries);
            newArrest = createNewArrestDB(inputArrestRequest, clientKey);
            switch (arrest.getOperation()){
                case INITIAL:
                    newArrest = dbManager.saveNewArrest(newArrest);
                    dbManager.addArrest2Client(clientKey, newArrest);
                    break;
                case EDIT:
                    ArrestDB arrestForEdit = dbManager.getSameArrestByClientKeyAndCurrentArrest(clientKey, newArrest, inputArrestRequest.getArrest().getRefDocNum());
                    arrestForEdit = editArrest(arrestForEdit, newArrest.getAmount(), newArrest.getArrestBaseDefinition());
                    newArrest = dbManager.updateArrest(arrestForEdit);
                    break;
                case CANCEL:
                    ArrestDB arrestForCancel = dbManager.getSameArrestByClientKeyAndCurrentArrest(clientKey, newArrest, inputArrestRequest.getArrest().getRefDocNum());
                    arrestForCancel.setArrestState(ArrestState.CANCELED);
                    newArrest = dbManager.updateArrest(arrestForCancel);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown Operation code!!!");
            }
        }else {
            throw new IllegalArgumentException("Unknown Format of identification document");
        }
        return newArrest;
    }

    private ArrestDB editArrest(ArrestDB sameArrest, long amount, String arrestBaseDefinition) {
        sameArrest.setAmount(amount);
        sameArrest.setArrestBaseDefinition(arrestBaseDefinition);
        if (amount > 0){
            sameArrest.setArrestState(ArrestState.ACTIVE);
        }else {sameArrest.setArrestState(ArrestState.CANCELED);}
        return sameArrest;
    }

    private ArrestDB createNewArrestDB(InputArrestRequest inputArrestRequest, ClientKey clientKey) {
        ArrestDB DBArrest = new ArrestDB();
        Arrest arrest = inputArrestRequest.getArrest();
        DBArrest.setDepartmentId(inputArrestRequest.getOrganCode());
        DBArrest.setDocumentDate(arrest.getDocDate());
        DBArrest.setPrimaryDocumentNumber(arrest.getDocNum());
        DBArrest.setArrestBaseDefinition(arrest.getPurpose());
        DBArrest.setAmount(arrest.getAmount());
        DBArrest.setArrestState(ArrestState.ACTIVE);
        DBArrest.setClientFirstName(clientKey.getFirstName());
        DBArrest.setClientLastName(clientKey.getLastName());
        DBArrest.setClientDocumentTypeId(clientKey.getDocumentTypeId());
        DBArrest.setClientNumberAndSeries(clientKey.getNumberAndSeries());
        return DBArrest;
    }
}

package Main.request;

import java.util.UUID;

public class InputArrestRequest {
    private UUID requestId;
    private String lastName;
    private String firstName;
    private IdentDoc identDoc;
    private int organCode;
    private Arrest arrest;

    public InputArrestRequest(UUID requestId, String lastName, String firstName, IdentDoc identDoc, Arrest arrest) {
        this.requestId = requestId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.identDoc = identDoc;
        this.arrest = arrest;
    }

    public InputArrestRequest() {
    }

    public UUID getRequestId() {
        return requestId;
    }

    public void setRequestId(UUID requestId) {
        this.requestId = requestId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public IdentDoc getIdentDoc() {
        return identDoc;
    }

    public void setIdentDoc(IdentDoc identDoc) {
        this.identDoc = identDoc;
    }

    public int getOrganCode() {
        return organCode;
    }

    public void setOrganCode(int organCode) {
        this.organCode = organCode;
    }

    public Arrest getArrest() {
        return arrest;
    }

    public void setArrest(Arrest arrest) {
        this.arrest = arrest;
    }
}

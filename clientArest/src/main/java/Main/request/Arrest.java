package Main.request;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class Arrest {
    public static final String DEFAULT_DOCUMENT_NUM_REFERENCE = "";
    @JsonFormat(pattern="dd.MM.yyyy")
    private Date docDate;
    private String docNum;
    private String purpose;
    private Long amount;
    private String refDocNum = DEFAULT_DOCUMENT_NUM_REFERENCE;
    private int operation;

    public Arrest(Date docDate, String docNum, String purpose, Long amount, String refDocNum, int operation) {
        this.docDate = docDate;
        this.docNum = docNum;
        this.purpose = purpose;
        this.amount = amount;
        this.refDocNum = refDocNum;
        this.operation = operation;
    }

    public Arrest(Date docDate, String docNum, String purpose, Long amount, int operation) {
        this(docDate, docNum, purpose, amount, DEFAULT_DOCUMENT_NUM_REFERENCE, operation);
    }

    public Arrest() {
    }

    public Date getDocDate() {
        return docDate;
    }

    public void setDocDate(Date docDate) {
        this.docDate = docDate;
    }

    public String getDocNum() {
        return docNum;
    }

    public void setDocNum(String docNum) {
        this.docNum = docNum;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getRefDocNum() {
        return refDocNum;
    }

    public void setRefDocNum(String refDocNum) {
        this.refDocNum = refDocNum;
    }

    public int getOperation() {
        return operation;
    }

    public void setOperation(int operation) {
        this.operation = operation;
    }
}

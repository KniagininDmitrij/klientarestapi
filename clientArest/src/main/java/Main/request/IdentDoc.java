package Main.request;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class IdentDoc {
    private int type;
    private String numberSeries;
    @JsonFormat(pattern="dd.MM.yyyy")
    private Date issueDate;

    public IdentDoc() {
    }

    public IdentDoc(int type, String numberSeries, Date issueDate) {
        this.type = type;
        this.numberSeries = numberSeries;
        this.issueDate = issueDate;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getNumberSeries() {
        return numberSeries;
    }

    public void setNumberSeries(String numberSeries) {
        this.numberSeries = numberSeries;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }
}
